UPDATE creature SET spawnmask = 6 WHERE map = 36;
UPDATE gameobject SET spawnmask = 6 WHERE map = 36;
UPDATE creature SET spawnmask = 2  WHERE id IN (46890,46903,46902,24935,46889,46906);

UPDATE creature_template SET scriptname = 'boss_foereaper5000' WHERE entry = 43778;
UPDATE creature_template SET scriptname = 'generic_creature' WHERE entry = 46890;
UPDATE creature_template SET scriptname = 'generic_creature' WHERE entry = 46906;
UPDATE creature_template SET scriptname = 'boss_glubtok' WHERE entry = 47162;
UPDATE creature_template SET scriptname = 'boss_helix_gearbreaker' WHERE entry = 47296;
UPDATE creature_template SET scriptname = 'npc_lumbering_oaf' WHERE entry = 47297;
UPDATE creature_template SET scriptname = 'boss_admiral_ripsnarl' WHERE entry = 47626;
UPDATE creature_template SET scriptname = 'boss_captain_cookie' WHERE entry = 47739;
UPDATE creature_template SET scriptname = 'boss_vanessa_vancleef' WHERE entry = 49541;
UPDATE creature_template SET scriptname = 'npc_admiral_ripsnarl_vapor' WHERE entry = 47714;

update creature_template set scriptname = 'generic_creature' where entry in (48229, 48230, 48262, 48278, 48279, 48284, 48338, 48351,48417,48418,48419,48420,48440,48441,48442,48445,48447,48450,48451,48502,48505,48521,48522,49670,49671,49674,49681);


