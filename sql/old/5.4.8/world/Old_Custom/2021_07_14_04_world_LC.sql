ALTER TABLE `spell_area` CHANGE `autocast` `flags` TINYINT(3) UNSIGNED NOT NULL DEFAULT '3' AFTER `gender`;
UPDATE `spell_area` SET `flags`=`flags`|2;

DELETE FROM `spell_area` WHERE `area`=5834;
INSERT INTO `spell_area` (`spell`, `area`, `quest_start`, `quest_end`, `aura_spell`, `racemask`, `gender`, `flags`, `quest_start_status`, `quest_end_status`) VALUES
('67789','5834','29524','29409','0','0','2','1','66','66'),
('59087','5834','0','29524','0','0','2','1','0','66');

DELETE FROM `smart_scripts` WHERE `entryorguid`=53566;
UPDATE `creature_template` SET `AIName`='' WHERE `entry`=53566;
DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=26;

UPDATE `creature` SET `phaseMask`=1 WHERE `id`=53566;
