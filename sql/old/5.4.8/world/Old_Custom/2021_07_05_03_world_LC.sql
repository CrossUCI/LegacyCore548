DELETE FROM `phase_definitions` WHERE `zoneId`=5736;
INSERT INTO `phase_definitions` (`zoneId`, `entry`, `phasemask`, `phaseId`, `terrainswapmap`, `flags`, `comment`) VALUES
('5736','1','1','0','0','0','Wandering Island - Default Phase'),
('5736','2','0','0','975','0','Wandering Island - Turtle Hurted'),
('5736','3','0','0','976','0','Wandering Island - Turtle Healed');

DROP TABLE IF EXISTS terrain_phase_info;
DROP TABLE IF EXISTS terrain_swap_defaults;
DROP TABLE IF EXISTS terrain_worldmap;
DROP TABLE IF EXISTS phase_area;

