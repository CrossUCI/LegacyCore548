UPDATE `creature_template` SET `ScriptName`='mob_master_shang_xi', `AIName`='' WHERE `entry`=53566;
UPDATE `gameobject_template` SET `ScriptName`='go_wandering_weapon_rack', `AIName`='' WHERE `entry`=210005;
UPDATE `creature_template` SET `ScriptName`='mob_training_target', `AIName`='' WHERE `entry` IN (53714,57873);
UPDATE `creature_template` SET `ScriptName`='mob_tushui_trainee', `AIName`='' WHERE `entry` IN (54587,65471);
UPDATE `creature_template` SET `ScriptName`='boss_jaomin_ro', `AIName`='' WHERE `entry`=54611;
UPDATE `gameobject_template` SET `flags`=0, `unkInt32`=0, `data0`=0,`data5`=1, `data13`=1 WHERE `entry` = 210986;

UPDATE `creature_template` SET `IconName` = 'openhand', `npcflag` =  npcflag | 16777216, `unit_flags` = '295680' WHERE `entry` = 53566;

DELETE FROM `npc_spellclick_spells` WHERE npc_entry = 53566;
INSERT INTO `npc_spellclick_spells` (`npc_entry`, `spell_id`, `cast_flags`, `user_type`) VALUES
(53566, 114746, 1, 1);

DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId` = 18 AND `SourceEntry` = 114746;
INSERT INTO `conditions` (`SourceTypeOrReferenceId`,`SourceGroup`,`SourceEntry`,`ConditionTypeOrReference`, `ConditionTarget`,`ConditionValue1`,`ConditionValue2`,`NegativeCondition`,`Comment`)
VALUES (18, 53566, 114746, 9, 0, 29408, 0, 0, 'Required quest active for spellclick');
