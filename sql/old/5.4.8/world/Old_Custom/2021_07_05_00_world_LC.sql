DROP TABLE IF EXISTS `phase_definitions`;
CREATE TABLE `phase_definitions` (
  `zoneId` int(11) unsigned NOT NULL DEFAULT '0',
  `entry` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `phasemask` int(11) unsigned NOT NULL DEFAULT '0',
  `phaseId` int(11) unsigned NOT NULL DEFAULT '0',
  `terrainswapmap` int(11) unsigned NOT NULL DEFAULT '0',
  `flags` tinyint(3) unsigned DEFAULT '0',
  `comment` text,
  PRIMARY KEY (`zoneId`,`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

ALTER TABLE `creature`
	ADD COLUMN `phaseMask` INT(10) UNSIGNED NOT NULL DEFAULT '0' AFTER `spawnMask`;


ALTER TABLE `gameobject`
   ADD COLUMN `phaseMask` INT(10) UNSIGNED NOT NULL DEFAULT '0' AFTER `spawnMask`;